# 2 EIP NAT
# 2 EIP BationesHostPub
# 2 NAT
# 1 Internet GateWay
# 1 VPC 
# 2 SubnetPub
# 2 SubnetPriv
# 4 RouteTable
# 1 AppLoadBalancer
# 1 TargetGroup
# 2 SG pub y priv
# 1 Keypair

# CREACION DE VPC
resource "aws_vpc" "example_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "VPC_Exercise"
  }
}

# KeyPair para bastiones
resource "aws_key_pair" "bastion_key" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChq9mfp3YpB61C84zDJbRhWBoPWIo+QLzXiieNSih91EmHQQZUF8rlXojXyyVRsNKQL98JoTtX5tk2qZjRQ0ahopY8rr9Owb6x2UblUy2EROgtMTEyVPH0Ex3HJoX6MtEBTrhFmL8WqFdPSyD90vojVJ2DmVlNA9jf9d+PJCiTOISmD9m80lfC6ByYkElLFmzJky9LBWpiz6u0oQY7v+PM6QEu8dWYO9l19jXqY3vIyjK2QQIxCmSGkLf80SW1tz7RjSna5waFOspXPbqRTiMwwEz5mRwngOfFrOs4b0ujIQxF97FobYihraEnp6vYDYA/7K4kdIkm8k4DStzGDMOAND/qqsvZT4hwzkD4IfEmOePLdpb4PwJZiZJZgvfHzhI28ChR/MSKyiRsPtXrAQr5dyE8vR9KfDJt2jPDO0f7rJo9DIoJgLbfT4wicPcG5qBoqfSX26teCSu2DkAOus/gGLYaCvdH99CB4J75wit/xb/+IEP+gSJ+kCkm3dCvgEk= yair rodriguez@DESKTOP-N1MJN6F"
} 

# CREACION DE SUBNETS PUBLICAS
resource "aws_subnet" "public_subnet_1" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-west-1a"
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-west-1c"
}

# CREACION DE SUBNETS PRIVADAS
resource "aws_subnet" "private_subnet_1" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-west-1a"
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "us-west-1c"
}

# Creacion de INTERNET Subnet publica
resource "aws_internet_gateway" "example_internet_gateway" {
  vpc_id = aws_vpc.example_vpc.id

  tags = {
    Name = "internet-gateway-subnet-public-vpc"
  }
}


# CREACION ELASTIC IP PARA NAT_1
resource "aws_eip" "nat_eip" {
  vpc = true
}

# CREACION ELASTIC IP PARA NAT_2
resource "aws_eip" "nat_eip_2" {
  vpc = true
}

# Creación de la puerta de enlace NAT para publica_1

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet_1.id
}

# Creación de la puerta de enlace NAT para publica_2

resource "aws_nat_gateway" "nat_gateway_2" {
  allocation_id = aws_eip.nat_eip_2.id
  subnet_id     = aws_subnet.public_subnet_2.id
}

# Creación de la tabla de enrutamiento para las subredes públicas_1
resource "aws_route_table" "public_route_table_1" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example_internet_gateway.id
  }

}

# Creación de la tabla de enrutamiento para las subredes públicas_2
resource "aws_route_table" "public_route_table_2" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example_internet_gateway.id
  }

}

# Asociación de las subredes públicas con la tabla de enrutamiento pública_1
resource "aws_route_table_association" "public_subnet_1_association" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_table_1.id
}

# Asociación de las subredes públicas con la tabla de enrutamiento pública_2
resource "aws_route_table_association" "public_subnet_2_association" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table_2.id
}

# creacion tabla de ruta para la subred privada_1
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
}

# creacion tabla de ruta para la subred privada_2
resource "aws_route_table" "private_route_table_2" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_2.id
  }
}


#ASOCIAR CADA SUBNET PRIVADA CON AWS_ROUTE_TABLE privada_1
resource "aws_route_table_association" "private_subnet_1_association" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_route_table.id
}

#ASOCIAR CADA SUBNET PRIVADA CON AWS_ROUTE_TABLE privada_2
resource "aws_route_table_association" "private_subnet_2_association" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_route_table_2.id
}


# Creación del grupo de seguridad para bastión SG
resource "aws_security_group" "bastion_sg" {
  vpc_id = aws_vpc.example_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2422
    to_port     = 2422
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# permitir conexion SSH solo de los host bastion SG
resource "aws_security_group" "nginx_sg" {
  vpc_id = aws_vpc.example_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.bastion_sg.id]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
}

# Creación de instancias EC2 para Nginx en subnets privadas
resource "aws_instance" "nginx_instance_1" {
  ami           = "ami-0f8e81a3da6e2510a"  
  instance_type = "t3.small"
  subnet_id     = aws_subnet.private_subnet_1.id
  key_name      = aws_key_pair.bastion_key.key_name


  associate_public_ip_address = false
 # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.nginx_sg.id
  ]

  tags = {
    "Name" = "nginx-instance-1"
  }
}

resource "aws_instance" "nginx_instance_2" {
  ami           = "ami-0f8e81a3da6e2510a"  
  instance_type = "t3.small"
  subnet_id     = aws_subnet.private_subnet_2.id
  key_name      = aws_key_pair.bastion_key.key_name


  associate_public_ip_address = false
 # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.nginx_sg.id
  ]

  tags = {
    "Name" = "nginx-instance-2"
  }
}

# Creación de instancias EC2 para Bastion en subnets pública_1

resource "aws_eip" "bastion_eip_1" {
  vpc      = true
  instance = aws_instance.bastion_instance_1.id
}

resource "aws_instance" "bastion_instance_1" {
  ami           = "ami-0f8e81a3da6e2510a"  
  instance_type = "t3.small"
  subnet_id     = aws_subnet.public_subnet_1.id
  key_name      = aws_key_pair.bastion_key.key_name


  # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.bastion_sg.id
  ]

  tags = {
    "Name" = "bastion-instance-1"
  }

}


#Creación de instancias EC2 para Bastion en subnets pública_2

resource "aws_eip" "bastion_eip_2" {
  vpc      = true
  instance = aws_instance.bastion_instance_2.id

}

resource "aws_instance" "bastion_instance_2" {
  ami           = "ami-0f8e81a3da6e2510a"  
  instance_type = "t3.small"
  subnet_id     = aws_subnet.public_subnet_2.id
  key_name      = aws_key_pair.bastion_key.key_name

  # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.bastion_sg.id
  ]

  tags = {
    "Name" = "bastion-instance-2"
  }
 
}

# Creación del Target Group para las instancias Nginx
resource "aws_lb_target_group" "nginx_target_group" {
  name        = "nginx-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.example_vpc.id
  target_type = "instance"

  health_check {
    path                = "/"
    protocol            = "HTTP"
    port                = "traffic-port"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }
}

# Registro de las instancias Nginx en el Target Group
resource "aws_lb_target_group_attachment" "nginx_attachment_1" {
  target_group_arn = aws_lb_target_group.nginx_target_group.arn
  target_id        = aws_instance.nginx_instance_1.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "nginx_attachment_2" {
  target_group_arn = aws_lb_target_group.nginx_target_group.arn
  target_id        = aws_instance.nginx_instance_2.id
  port             = 80
}

# Creación del Application Load Balancer (ALB)
resource "aws_lb" "alb" {
  name               = "nginx-alb"
  internal           = true
  load_balancer_type = "application"
  subnets            = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]

  # Configuración del Security Group del ALB
  security_groups = [aws_security_group.alb_sg.id]

  tags = {
    Name = "nginx-alb"
  }

}
# Creación del Listener para el ALB
resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nginx_target_group.arn
  }
}

# Creación del Security Group para el ALB
resource "aws_security_group" "alb_sg" {
  name_prefix = "alb-sg-"
  vpc_id      = aws_vpc.example_vpc.id  


  # Permitir tráfico HTTP (puerto 80) desde cualquier origen
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Permitir tráfico saliente hacia cualquier destino y cualquier puerto
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

