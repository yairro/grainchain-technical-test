[![pipeline status](https://gitlab.com/yairro/grainchain-technical-test/badges/main/pipeline.svg)](https://gitlab.com/yairro/grainchain-technical-test/-/commits/main)

# Complete CI/CD with Terraform + Ansible + Gitlab CI-CD

![Descripción de la imagen](./diagrama_1.png)
```
/
├── main.tf
├── provider.tf
├── variables.tf
├── output.tf
├── README.md
├── estado.jpg
└── .gitlab-ci.yaml
```

## Notas del ejercicio
- Se ocupo una imagen de Docker willhallonline/ansible para el Pipeline en Branch Ansible para ejecutar ansible en el Pipeline , ejecutar el Playbook e instalar Ansible en el Host Bastion para configurar Host Privados
- Se habilito el puerto 80 en los host bastiones para acceder a internet fuera de la zona y acceder por ssh al EC2 y ejecutar el Playbook de Ansible.
- Los Host Nginx tienen un ALB que escucha por el puerto 80 los EC2 de las Subnets privadas
- Los Host Nginx solo tienen IP Privada, pero pueden acceder a internet por NAT
- Los Host Nginx solo tienen acceso por SSH con los Host Bastiones
- Se obtienen las IPs Privadas en el Output del Pipeline Main y se actualiza el inventorypriv.yaml
- Desde el Host Bastion se ejecuta inventory.priv yaml

# Testear
- En el output del Pipeline Main, se obtienen las EIP, IPS Privadas y ALB 
- Todas las maquinas tienen el mismo keychain ssh
- La clave privada se encuentra en el output del pipeline de la rama Ansible
- Cambiar permisos de la clave chmod 400 
- ssh -i clave ubuntu@<ip>
- La ALB solo obtiene output en la VPC ya es que es de tipo internal.

## 🏆 Test NAT e IGW (Conexion a Internet)
```hcl
#Dentro de un EC2 de SubnetPrivada sin IP Publica
sudo apt update
output:
Get:41 http://security.ubuntu.com/ubuntu jammy-security/multiverse Translation-en [7060 B]                      
Get:42 http://security.ubuntu.com/ubuntu jammy-security/multiverse amd64 c-n-f Metadata [260 B]
Fetched 26.0 MB in 5s (5501 kB/s)               
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done

}
```

## 🏆 Test SSH Host Bastion a Host Privado Puerto 22 
```hcl
#Dentro de un EC2 Subnet Publica
chmod 400 llave

ssh -i clave ubuntu@<ip_privada>

}
```

## 🏆 Test ALB En Subnet con DNS
```hcl
# Dentro de un EC2 Subnet Publica

curl internal-nginx-alb-1397471892.us-east-1.elb.amazonaws.com

}
```
## 🏆 Test NGINX en EC2 Subnets
```hcl
# Dentro de un EC2 Subnet Privada

nginx -v
curl localhost
output:
!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
}
```

## Technologies:
- Terraform
- Gitlabk CI/CD
- AWS EC2
- AWS Security Groups
- AWS VPS
- Docker

# Master Branch 🔨 : Exercise 1
´
root
main.tf provider.tf variables.tf


# 🐋 IMAGENES 
  - willhallonline/ansible # Ansible en un contenedor para CI CD
  - alpine/k8s:1.24.15 # Una imagen Docker que incluya kubectl y AWS CLI  

# 🚀 Ejecución del GitLab CI/CD para desplegar la arquitectura de Terraform en AWS
El proceso de despliegue de la arquitectura de Terraform en AWS a través de GitLab CI/CD se puede resumir en los siguientes pasos:

**Hacer un Commit**: El primer paso es hacer un commit en la rama main del repositorio que contiene el código de infraestructura en Terraform. Al hacer un commit, se generará un evento que actuará como trigger para el GitLab CI/CD.

**GitLab CI/CD Pipeline**: Al realizar el commit en la rama main, el GitLab CI/CD detectará este cambio y ejecutará automáticamente el pipeline configurado para esa rama. El pipeline se define en un archivo llamado .gitlab-ci.yml en el repositorio, y en este archivo se encuentran todas las configuraciones y pasos que se deben seguir durante el proceso de despliegue.

**Proceso de Despliegue**: El pipeline configurado en el archivo .gitlab-ci.yml consta de múltiples etapas y trabajos. Cada trabajo representa una tarea específica que se debe realizar durante el proceso de despliegue. Estos trabajos se ejecutan en un entorno aislado y controlado por GitLab CI/CD.

**Variables de Entorno**: Antes de iniciar el proceso de despliegue, se deben configurar las variables de entorno necesarias en GitLab CI/CD. Estas variables, como las credenciales de AWS y las claves SSH, se almacenan de forma segura como secretos y se utilizan para autenticar y acceder a los recursos de AWS durante el despliegue.

**Despliegue de la Infraestructura**: Durante el proceso de despliegue, Terraform se encargará de crear y configurar la infraestructura en AWS según las especificaciones definidas en el código de Terraform. Esto puede incluir la creación de VPC, subredes, grupos de seguridad, instancias EC2 y otros recursos de AWS.


**Notificaciones y Resultados**: Una vez finalizado el proceso de despliegue, GitLab CI/CD puede enviar notificaciones sobre el resultado del pipeline, informando si el despliegue fue exitoso o si hubo algún problema durante el proceso.


Este flujo de trabajo permite a los equipos de desarrollo implementar, administrar y mantener la infraestructura en AWS de manera eficiente y confiable. GitLab CI/CD automatiza el proceso de despliegue, lo que garantiza la consistencia y reduce los errores humanos, al tiempo que proporciona un entorno controlado para las pruebas y validaciones.

# 🛠️ Administración de Configuración con Ansible en la rama ansible
Además del despliegue de la infraestructura con Terraform en la rama main, el repositorio también cuenta con una rama llamada **ansible** que se utiliza para la administración de la configuración de las máquinas EC2 recién desplegadas. Para lograr esto, se ha configurado un pipeline específico para la rama ansible que ejecuta un stage llamado **ansibleConfig**.

**Rama Ansible y Pipeline**
En la rama ansible, se encuentra todo el código y las playbooks de Ansible necesarios para la administración de la configuración de las máquinas EC2. Ansible es una herramienta de administración de configuración que permite automatizar tareas de configuración, instalación de software y otras operaciones en los servidores.

**Pipeline para el Stage AnsibleConfig**
Cuando se realiza un commit en la rama ansible, el GitLab CI/CD detecta el cambio y ejecuta el pipeline correspondiente. En este caso, el pipeline contiene un stage específico llamado ansibleConfig.

**Stage AnsibleConfig**
El stage ansibleConfig está diseñado para configurar las máquinas EC2 recién desplegadas utilizando las playbooks de Ansible definidas en la rama ansible. Estas playbooks contienen todas las tareas necesarias para instalar y configurar software, ajustar la configuración del sistema operativo, y realizar otras tareas de administración en las instancias EC2.

**Beneficios de Ansible en el Pipeline**
Utilizar Ansible como parte del pipeline en la rama ansible brinda numerosos beneficios:

**Automatización de la Configuración:** Ansible automatiza la configuración y las tareas de administración en las máquinas EC2, lo que garantiza que todas las instancias tengan la misma configuración y software instalado de manera coherente.

**Gestión Centralizada:** Las playbooks de Ansible se gestionan centralmente en el repositorio, lo que facilita la actualización y el mantenimiento de la configuración en todas las instancias.

**Reusabilidad y Modularidad:** Ansible permite crear playbooks reutilizables y módulos, lo que agiliza la configuración de nuevas instancias y simplifica la administración de configuraciones complejas.

**Auditoría y Control:** La utilización de Ansible en el pipeline proporciona un mayor control y auditoría sobre las acciones y cambios realizados en las instancias EC2, lo que mejora la seguridad y el cumplimiento.

Resultados y Notificaciones
Al finalizar el stage ansibleConfig, GitLab CI/CD notifica sobre el resultado de la configuración de las máquinas EC2. En caso de algún problema o error durante la ejecución de las playbooks de Ansible, las notificaciones permiten identificar y solucionar rápidamente cualquier inconveniente.

El uso de Ansible en el pipeline de la rama ansible proporciona una solución completa para el despliegue y administración de la infraestructura en AWS, lo que permite mantener un control total sobre la configuración de las máquinas EC2 y garantizar que se encuentren correctamente configuradas y listas para su uso.

## Tasks:

# 🚀 Variables necesarias para GitLab CI/CD

En este caso, se deben agregar las siguientes variables de entorno a GitLab CI/CD para permitir la ejecución de los despliegues y tareas relacionadas con infraestructura en AWS y otras configuraciones:

1. **AWS_ACCESS_KEY_ID**: Es la clave de acceso de la cuenta de AWS que GitLab CI/CD utilizará para autenticarse y acceder a los recursos en la nube.

2. **AWS_SECRET_ACCESS_KEY**: Es la clave secreta asociada a la clave de acceso de AWS. Esta clave se utiliza en combinación con el ID de acceso para realizar operaciones en AWS.

3. **AWS_REGION**: Es la región de AWS en la que se desplegarán los recursos. Esta variable permite que GitLab CI/CD sepa en qué región debe ejecutar las tareas.

4. **AWS_TF_STATE_BUCKET_NAME**: Es el nombre del bucket de Amazon S3 que se utilizará para almacenar el estado del backend de Terraform. Esta variable es esencial para mantener el estado de Terraform en un lugar seguro y compartido entre diferentes ejecuciones.

5. **SSH_PRIVATE_KEY**: Es la clave privada utilizada para acceder a instancias EC2 o para realizar otras tareas que requieran autenticación SSH.

6. **SSH_PUBLIC_KEY**: Es la clave pública que corresponde a la clave privada anterior. Se utiliza para permitir la autenticación a través de SSH en las instancias o recursos que la requieran.

Agregar estas variables como secretos en GitLab CI/CD asegurará que la información sensible, como claves de acceso y claves privadas, se mantenga encriptada y protegida durante las operaciones de CI/CD.

# ⚒️ Create main.tf file with the following code blocks

# CREACIÓN DE VPC

En este bloque de código se crea una Virtual Private Cloud (VPC) en AWS. La VPC tiene un rango de direcciones IP (CIDR) de 10.0.0.0/16 y se le asigna el nombre "VPC_Exercise".

```hcl
resource "aws_vpc" "example_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "VPC_Exercise"
  }
}
```
# 🔑 KeyPair para bastiones

Este bloque de código crea un par de claves (KeyPair) para los bastiones en AWS. La clave tiene el nombre "deployer-key" y se especifica la clave pública asociada.

```hcl
resource "aws_key_pair" "bastion_key" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQChq9mfp3YpB61C84zDJbRhWBoPWIo+QLzXiieNSih91EmHQQZUF8rlXojXyyVRsNKQL98JoTtX5tk2qZjRQ0ahopY8rr9Owb6x2UblUy2EROgtMTEyVPH0Ex3HJoX6MtEBTrhFmL8WqFdPSyD90vojVJ2DmVlNA9jf9d+PJCiTOISmD9m80lfC6ByYkElLFmzJky9LBWpiz6u0oQY7v+PM6QEu8dWYO9l19jXqY3vIyjK2QQIxCmSGkLf80SW1tz7RjSna5waFOspXPbqRTiMwwEz5mRwngOfFrOs4b0ujIQxF97FobYihraEnp6vYDYA/7K4kdIkm8k4DStzGDMOAND/qqsvZT4hwzkD4IfEmOePLdpb4PwJZiZJZgvfHzhI28ChR/MSKyiRsPtXrAQr5dyE8vR9KfDJt2jPDO0f7rJo9DIoJgLbfT4wicPcG5qBoqfSX26teCSu2DkAOus/gGLYaCvdH99CB4J75wit/xb/+IEP+gSJ+kCkm3dCvgEk= yair rodriguez@DESKTOP-N1MJN6F"
}
```


# 🌐 CREACIÓN DE SUBNETS PUBLICAS

En este bloque de código se crean dos subnets públicas en AWS para la VPC previamente creada. Cada subnet tiene un rango de direcciones IP (CIDR) y está asociada a dos zonas de disponibilidad (Availability Zone) en la región "us-east-1a y us-east-1b".

## Subnet pública 1 y 2 

```hcl
# CREACION DE SUBNETS PUBLICAS
resource "aws_subnet" "public_subnet_1" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
}

resource "aws_subnet" "public_subnet_2" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"
}

```

# 🌐 CREACIÓN DE SUBNETS PRIVADAS

En este bloque de código se crean dos subnets públicas en AWS para la VPC previamente creada. Cada subnet tiene un rango de direcciones IP (CIDR) y está asociada a dos zonas de disponibilidad (Availability Zone) en la región "us-eas-1a y us-eas-1b".

## Subnet privada 1 y 2 


```hcl
# CREACION DE SUBNETS PRIVADAS
resource "aws_subnet" "private_subnet_1" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "us-east-1a"
}

resource "aws_subnet" "private_subnet_2" {
  vpc_id            = aws_vpc.example_vpc.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "us-east-1b"
}

```

# 🌐 Creación de INTERNET Subnet pública

En este bloque de código se crea una puerta de enlace de Internet (Internet Gateway) en AWS para la VPC previamente creada. La puerta de enlace de Internet permitirá que las instancias ubicadas en las subnets públicas puedan comunicarse con Internet.

```hcl
resource "aws_internet_gateway" "example_internet_gateway" {
  vpc_id = aws_vpc.example_vpc.id

  tags = {
    Name = "internet-gateway-subnet-public-vpc"
  }
}

```

# 🔒 CREACION ELASTIC IP PARA NAT_1

En este bloque de código se crea una Elastic IP (EIP) en AWS para ser asociada a un recurso NAT (Network Address Translation) en la VPC. La EIP se configura para trabajar en una VPC.

La Elastic IP (EIP) creada en este bloque se utilizará para proporcionar una dirección IP pública estable y fija para el recurso NAT.

```hcl
resource "aws_eip" "nat_eip" {
  vpc = true
}

```

# 🔒 CREACION ELASTIC IP PARA NAT_2
En este bloque de código se crea otra Elastic IP (EIP) en AWS para ser asociada a un segundo recurso NAT (Network Address Translation) en la VPC. Al igual que la anterior, esta EIP también se configura para trabajar en una VPC.

```hcl
# CREACION ELASTIC IP PARA NAT_2
resource "aws_eip" "nat_eip_2" {
  vpc = true
}

```

# 🔒 Creación de la puerta de enlace NAT para publica_1

En este bloque de código se crea una puerta de enlace NAT (NAT Gateway) en AWS para la subnet `public_subnet_1`. La puerta de enlace NAT permitirá que las instancias ubicadas en la subnet pública puedan comunicarse con Internet a través de la Elastic IP (EIP) asociada.

```hcl
resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = aws_subnet.public_subnet_1.id
}

```

# 🔒 Creación de la puerta de enlace NAT para publica_2

En este bloque de código se crea una puerta de enlace NAT (NAT Gateway) en AWS para la subnet `public_subnet_2`. La puerta de enlace NAT permitirá que las instancias ubicadas en la subnet pública puedan comunicarse con Internet a través de la Elastic IP (EIP) asociada.

```hcl
resource "aws_nat_gateway" "nat_gateway_2" {
  allocation_id = aws_eip.nat_eip_2.id
  subnet_id     = aws_subnet.public_subnet_2.id
}

```

# 🌐 Creación de la tabla de enrutamiento para las subredes públicas_1

En este bloque de código se crea una tabla de enrutamiento (Route Table) en AWS para las subredes públicas. La tabla de enrutamiento se asociará a la VPC `example_vpc` previamente creada y permitirá el tráfico hacia Internet.

```hcl
resource "aws_route_table" "public_route_table_1" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example_internet_gateway.id
  }
}
```

# 🌐 Creación de la tabla de enrutamiento para las subredes públicas_2
En este bloque de código se crea una tabla de enrutamiento (Route Table) en AWS para las subredes públicas. La tabla de enrutamiento se asociará a la VPC `example_vpc` previamente creada y permitirá el tráfico hacia Internet.

```hcl
resource "aws_route_table" "public_route_table_2" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.example_internet_gateway.id
  }

}

```

# 🌐 Asociación de las subredes públicas con la tabla de enrutamiento pública_1

En este bloque de código se realiza la asociación entre la subred pública `public_subnet_1` y la tabla de enrutamiento `public_route_table_1`. Esto permitirá que la subred pública pueda utilizar la tabla de enrutamiento para dirigir el tráfico hacia Internet.

```hcl
resource "aws_route_table_association" "public_subnet_1_association" {
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.public_route_table_1.id
}
```

# 🌐 Asociación de las subredes públicas con la tabla de enrutamiento pública_2

En este bloque de código se realiza la asociación entre la subred pública `public_subnet_2` y la tabla de enrutamiento `public_route_table_2`. Esto permitirá que la subred pública pueda utilizar la tabla de enrutamiento para dirigir el tráfico hacia Internet.

```hcl
resource "aws_route_table_association" "public_subnet_2_association" {
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.public_route_table_2.id
}

```

# 🔐 Creación de tabla de ruta para la subred privada_1

En este bloque de código se crea una tabla de enrutamiento (Route Table) en AWS para la subred privada `private_subnet_1`. La tabla de enrutamiento se asociará a la VPC `example_vpc` previamente creada y se configurará para redirigir todo el tráfico hacia el NAT Gateway `nat_gateway`.

```hcl
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
}
```
# 🔐 Creación de tabla de ruta para la subred privada_2

En este bloque de código se crea una tabla de enrutamiento (Route Table) en AWS para la subred privada `private_subnet_2`. La tabla de enrutamiento se asociará a la VPC `example_vpc` previamente creada y se configurará para redirigir todo el tráfico hacia el NAT Gateway `nat_gateway`.

```hcl
resource "aws_route_table" "private_route_table_2" {
  vpc_id = aws_vpc.example_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_2.id
  }
}

```

# 🔐 Asociar cada subred privada con AWS_ROUTE_TABLE privada_1

En este bloque de código se realiza la asociación entre la subred privada `private_subnet_1` y la tabla de enrutamiento `private_route_table`. Esto permitirá que la subred privada `private_subnet_1` pueda utilizar la tabla de enrutamiento `private_route_table` para dirigir el tráfico hacia Internet a través de su correspondiente NAT Gateway `nat_gateway`.

```hcl
resource "aws_route_table_association" "private_subnet_1_association" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_route_table.id
}
```
# 🔐 Asociar cada subred privada con AWS_ROUTE_TABLE privada_2

En este bloque de código se realiza la asociación entre la subred privada `private_subnet_2` y la tabla de enrutamiento `private_route_table`. Esto permitirá que la subred privada `private_subnet_2` pueda utilizar la tabla de enrutamiento `private_route_table` para dirigir el tráfico hacia Internet a través de su correspondiente NAT Gateway `nat_gateway`.

```hcl
#ASOCIAR CADA SUBNET PRIVADA CON AWS_ROUTE_TABLE privada_2
resource "aws_route_table_association" "private_subnet_2_association" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_route_table_2.id
}
```

# 🔐 Creación del grupo de seguridad para bastión SG

En este bloque de código se crea un grupo de seguridad (Security Group) en AWS para el bastión (bastion). El grupo de seguridad, llamado `bastion_sg`, se asociará a la VPC `example_vpc` previamente creada.

```hcl
resource "aws_security_group" "bastion_sg" {
  vpc_id = aws_vpc.example_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 2422
    to_port     = 2422
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

```

# 🔐 Permitir conexión SSH solo desde el grupo de seguridad del bastión SG

En este bloque de código se crea un grupo de seguridad (Security Group) en AWS para las instancias Nginx. El grupo de seguridad, llamado `nginx_sg`, se asociará a la VPC `example_vpc` previamente creada.

```hcl
resource "aws_security_group" "nginx_sg" {
  vpc_id = aws_vpc.example_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = [aws_security_group.bastion_sg.id]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  
}

```

# 🖥️ Creación de instancias EC2 para Nginx en subnets privadas

En este bloque de código se crean instancias EC2 en AWS para Nginx. Las instancias se lanzarán en las subredes privadas correspondientes.

```hcl
# Creación de instancias EC2 para Nginx en subnets privadas
resource "aws_instance" "nginx_instance_1" {
  ami           = "ami-053b0d53c279acc90"  
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_subnet_1.id
  key_name      = aws_key_pair.bastion_key.key_name


  associate_public_ip_address = false
 # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.nginx_sg.id
  ]

  tags = {
    "Name" = "nginx-instance-1"
  }
}

resource "aws_instance" "nginx_instance_2" {
  ami           = "ami-053b0d53c279acc90"  
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_subnet_2.id
  key_name      = aws_key_pair.bastion_key.key_name


  associate_public_ip_address = false
 # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.nginx_sg.id
  ]

  tags = {
    "Name" = "nginx-instance-2"
  }
}

```

# 🔑 Creación de instancias EC2 para Bastion en subnets pública_1 y pública_2
En este bloque de código se crean dos instancias EC2 para el bastión (bastion) en las subredes públicas public_subnet_1 y public_subnet_2. Estas instancias permitirán administrar y acceder a otras instancias en la VPC.

```hcl
# Creación de instancias EC2 para Bastion en subnets pública_1

resource "aws_eip" "bastion_eip_1" {
  vpc      = true
  instance = aws_instance.bastion_instance_1.id
}

resource "aws_instance" "bastion_instance_1" {
  ami           = "ami-053b0d53c279acc90"  
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.public_subnet_1.id
  key_name      = aws_key_pair.bastion_key.key_name


  # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.bastion_sg.id
  ]

  tags = {
    "Name" = "bastion-instance-1"
  }

}


#Creación de instancias EC2 para Bastion en subnets pública_2

resource "aws_eip" "bastion_eip_2" {
  vpc      = true
  instance = aws_instance.bastion_instance_2.id

}

resource "aws_instance" "bastion_instance_2" {
  ami           = "ami-053b0d53c279acc90"  
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.public_subnet_2.id
  key_name      = aws_key_pair.bastion_key.key_name

  # Configuración de las reglas de seguridad
  vpc_security_group_ids = [
    aws_security_group.bastion_sg.id
  ]

  tags = {
    "Name" = "bastion-instance-2"
  }
 
}

```

# Creación del Target Group para las instancias Nginx

Este bloque de código crea un grupo de destino (Target Group) para las instancias Nginx. El Target Group define cómo el Application Load Balancer (ALB) dirigirá el tráfico a las instancias.

```hcl
resource "aws_lb_target_group" "nginx_target_group" {
  name        = "nginx-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.example_vpc.id
  target_type = "instance"

  health_check {
    path                = "/"
    protocol            = "HTTP"
    port                = "traffic-port"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }
}
```

# Registro de las instancias Nginx en el Target Group
En este bloque, se registran las instancias Nginx en el Target Group creado anteriormente. Esto permite que el ALB dirija el tráfico entrante a estas instancias.

```hcl

resource "aws_lb_target_group_attachment" "nginx_attachment_1" {
  target_group_arn = aws_lb_target_group.nginx_target_group.arn
  target_id        = aws_instance.nginx_instance_1.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "nginx_attachment_2" {
  target_group_arn = aws_lb_target_group.nginx_target_group.arn
  target_id        = aws_instance.nginx_instance_2.id
  port             = 80
}
```

# Creación del Application Load Balancer (ALB)
En este bloque, se crea el Application Load Balancer (ALB) que distribuirá el tráfico entre las instancias Nginx registradas en el Target Group.

```hcl
resource "aws_lb" "alb" {
  name               = "nginx-alb"
  internal           = true
  load_balancer_type = "application"
  subnets            = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]

  # Configuración del Security Group del ALB
  security_groups = [aws_security_group.alb_sg.id]

  tags = {
    Name = "nginx-alb"
  }
}
```
# Creación del Listener para el ALB
Este bloque configura un Listener para el ALB, que especifica cómo manejar el tráfico entrante en el puerto 80 (HTTP) y redirigirlo al Target Group de las instancias Nginx.

```hcl
resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nginx_target_group.arn
  }
}
```
# Creación del Security Group para el ALB
En este bloque, se crea un Security Group para el ALB, que controla el tráfico entrante y saliente del balanceador de carga. Aquí se permite el tráfico HTTP entrante desde cualquier origen y el tráfico saliente hacia cualquier destino.

```hcl
resource "aws_security_group" "alb_sg" {
  name_prefix = "alb-sg-"
  vpc_id      = aws_vpc.example_vpc.id  

  # Permitir tráfico HTTP (puerto 80) desde cualquier origen
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Permitir tráfico saliente hacia cualquier destino y cualquier puerto
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```
