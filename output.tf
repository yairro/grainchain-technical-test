# Output para las direcciones IP de las instancias Nginx
output "nginx_instance_1_ip" {
  value = aws_instance.nginx_instance_1.private_ip
}

output "nginx_instance_2_ip" {
  value = aws_instance.nginx_instance_2.private_ip
}

# Output para las direcciones IP de las instancias Bastion
output "bastion_instance_1_ip" {
  value = aws_instance.bastion_instance_1.public_ip
}

output "bastion_instance_2_ip" {
  value = aws_instance.bastion_instance_2.public_ip
}

# Output para las direcciones IP de los Application Load Balancer
output "alb_dns_name" {
  value = aws_lb.alb.dns_name
}

output "alb_zone_id" {
  value = aws_lb.alb.zone_id
}

# Output para las direcciones IP de los NAT Gateways
output "nat_gateway_1_eip" {
  value = aws_eip.nat_eip.public_ip
}

output "nat_gateway_2_eip" {
  value = aws_eip.nat_eip_2.public_ip
}

# Output para las direcciones IP de las Elastic IPs de los Bastiones
output "bastion_eip_1" {
  value = aws_eip.bastion_eip_1.public_ip
}

output "bastion_eip_2" {
  value = aws_eip.bastion_eip_2.public_ip
}
